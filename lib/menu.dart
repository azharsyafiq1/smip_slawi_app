import 'package:flutter/material.dart';
import 'package:smip_slawi_app/Notifikasi.dart';
import 'package:smip_slawi_app/akun.dart';
import 'package:smip_slawi_app/welcome.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
          child: Row(
        children: [
          buildItemAppbar(Icons.home, 0),
          buildItemAppbar(Icons.person, 1),
          buildItemAppbar(Icons.notifications, 2),
        ],
      )),
      body: Stack(children: [
        Offstage(
            offstage: _selectedIndex != 0,
            child: TickerMode(
              enabled: _selectedIndex == 0,
              child: WELCOME(),
            )),
        Offstage(
            offstage: _selectedIndex != 1,
            child: TickerMode(
              enabled: _selectedIndex == 1,
              child: Akun(),
            )),
        Offstage(
            offstage: _selectedIndex != 2,
            child: TickerMode(
              enabled: _selectedIndex == 2,
              child: Notifikasi(),
            ))
      ]),
    );
  }

  Widget buildItemAppbar(IconData icon, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
      },
      child: Container(
          height: 50,
          width: MediaQuery.of(context).size.width / 3,
          decoration: index == _selectedIndex
              ? BoxDecoration(shape: BoxShape.circle, color: Colors.blue)
              : BoxDecoration(),
          child: Icon(
            icon,
            color: index == _selectedIndex ? Colors.white : Colors.grey,
          )),
    );
  }
}
