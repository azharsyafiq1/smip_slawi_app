import 'package:flutter/material.dart';

import 'package:smip_slawi_app/menu.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Menu(),
  ));
}
