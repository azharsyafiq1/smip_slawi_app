import 'package:flutter/material.dart';

class Notifikasi extends StatefulWidget {
  @override
  _NotifikasiState createState() => _NotifikasiState();
}

class _NotifikasiState extends State<Notifikasi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Notifikasi")),
        actions: [Icon(Icons.notifications)],
      ),
      body: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Container(
                width: 20,
                height: 100,
                color: Colors.blue,
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                height: 100,
                width: 300,
                decoration: BoxDecoration(color: Colors.grey.withOpacity(0.2)),
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Container(
                width: 20,
                height: 100,
                color: Colors.blue,
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                height: 100,
                width: 300,
                decoration: BoxDecoration(color: Colors.grey.withOpacity(0.2)),
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Container(
                width: 20,
                height: 100,
                color: Colors.blue,
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                height: 100,
                width: 300,
                decoration: BoxDecoration(color: Colors.grey.withOpacity(0.2)),
              ),
            ],
          )
        ],
      ),
    );
  }
}
