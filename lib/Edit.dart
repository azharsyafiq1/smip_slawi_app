import 'package:flutter/material.dart';

import 'package:smip_slawi_app/menu.dart';

class Edit extends StatefulWidget {
  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.blue[400], Colors.white])),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                width: double.infinity,
                child: Padding(
                  padding: EdgeInsets.all(30.0),
                  child: Column(children: [
                    SizedBox(
                      height: 70,
                    ),
                    Stack(
                      children: [
                        Image.asset(
                          "assets/image/akun1.png",
                          height: 80,
                        ),
                        Positioned(
                            left: 50,
                            height: 130,
                            child: Icon(Icons.photo_camera))
                      ],
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: "NAMA",
                        hintText: "NAMA ANDA",
                        suffixIcon: Icon(Icons.account_circle),
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: "EMAIL",
                        hintText: "Email ANDA",
                        suffixIcon: Icon(Icons.mail),
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: "NO telepon",
                        hintText: "NO telepon anda",
                        suffixIcon: Icon(Icons.phone),
                      ),
                    ),
                    SizedBox(height: 30),
                    FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Colors.blue,
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Menu()));
                        },
                        child: Text(
                          "Submit?",
                          style: TextStyle(color: Colors.white),
                        )),
                  ]),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
