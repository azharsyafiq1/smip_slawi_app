import 'package:flutter/material.dart';
import 'package:smip_slawi_app/akun.dart';

class WELCOME extends StatefulWidget {
  @override
  _WELCOMEState createState() => _WELCOMEState();
}

class _WELCOMEState extends State<WELCOME> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            // backgroundColor: Colors.deepPurple[900],
            resizeToAvoidBottomInset: false,
            body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.blue[400], Colors.white])),
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.only(right: 12, left: 12),
                      child: Column(children: [
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: [
                            Text(
                              "Halo Syafiqq",
                              style:
                                  TextStyle(fontSize: 25, color: Colors.white),
                            ),
                            Spacer(),
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Akun()));
                                },
                                child: Image.asset("assets/image/akun1.png",
                                    height: 35)),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 60,
                                    width: 60,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: Colors.blue[800]),
                                    child: Padding(
                                      padding: EdgeInsets.all(12),
                                      child: Image.asset(
                                        "assets/image/hotel.png",
                                        fit: BoxFit.cover,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "Sunrise \nhotel",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                      height: 60,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: Colors.blue[800]),
                                      child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                            "assets/image/plane.png",
                                            fit: BoxFit.cover,
                                            color: Colors.white,
                                          ))),
                                  Text(
                                    "Smip \ntravel",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                      height: 60,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: Colors.blue[800]),
                                      child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                            "assets/image/computer.png",
                                            fit: BoxFit.cover,
                                            color: Colors.white,
                                          ))),
                                  Text(
                                    "Home \ncomputer",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                      height: 60,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: Colors.blue[800]),
                                      child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                            "assets/image/restaurant.png",
                                            fit: BoxFit.cover,
                                            color: Colors.white,
                                          ))),
                                  Text(
                                    "Kedai \nsedepan",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ]),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 60,
                                    width: 60,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: Colors.blue[800]),
                                    child: Padding(
                                      padding: EdgeInsets.all(12),
                                      child: Image.asset(
                                        "assets/image/banknote.png",
                                        fit: BoxFit.cover,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "Smip \nBank",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                      height: 60,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: Colors.blue[800]),
                                      child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                            "assets/image/laundry.png",
                                            fit: BoxFit.cover,
                                            color: Colors.white,
                                          ))),
                                  Text(
                                    "Smip \nlaundry",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                      height: 60,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: Colors.blue[800]),
                                      child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                            "assets/image/student.png",
                                            fit: BoxFit.cover,
                                            color: Colors.white,
                                          ))),
                                  Text(
                                    "Smip \nstudent",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                      height: 60,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: Colors.blue[800]),
                                      child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                            "assets/image/school.png",
                                            fit: BoxFit.cover,
                                            color: Colors.white,
                                          ))),
                                  Text(
                                    "      \nabout!",
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ]),
                        SizedBox(height: 20),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(children: [
                            Container(
                              height: 100,
                              width: 220,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.blue),
                              child: Padding(
                                padding: EdgeInsets.all(12),
                                child: Text(
                                  "50% OFF \nSunrise hotel",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 25),
                                ),
                              ),
                            ),
                            SizedBox(width: 12),
                            Container(
                              height: 100,
                              width: 220,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.blue),
                              child: Padding(
                                padding: EdgeInsets.all(12),
                                child: Text(
                                  "30% OFF \nSMIP Travel",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 25),
                                ),
                              ),
                            ),
                            SizedBox(width: 12),
                            Container(
                              height: 100,
                              width: 220,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.blue),
                              child: Padding(
                                padding: EdgeInsets.all(12),
                                child: Text(
                                  "30% OFF \nKedai sedepan",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 25),
                                ),
                              ),
                            ),
                            SizedBox(width: 12),
                            Container(
                              height: 100,
                              width: 220,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.blue),
                              child: Padding(
                                padding: EdgeInsets.all(12),
                                child: Text(
                                  "30% OFF \nSMIP laundry",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 25),
                                ),
                              ),
                            )
                          ]),
                        )
                      ])),
                ],
              ),
            )));
  }
}
