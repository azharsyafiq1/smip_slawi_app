import 'package:flutter/material.dart';
import 'package:smip_slawi_app/menu.dart';

import 'package:smip_slawi_app/register.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.blue[400], Colors.white])),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.all(20),
                    child: SizedBox(
                        width: double.infinity,
                        child: Column(children: [
                          SizedBox(
                            height: 50,
                          ),
                          Image.asset("assets/image/smip.png", height: 100),
                          Text(
                            "SMIP SLAWI",
                            style: TextStyle(fontSize: 40),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Username",
                                  hintText: "Masukan username anda",
                                  suffixIcon: Icon(Icons.account_circle),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20)))),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                              obscureText: true,
                              decoration: InputDecoration(
                                  labelText: "Password",
                                  hintText: "Masukan password anda",
                                  suffixIcon: Icon(Icons.lock),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(20)))),
                          SizedBox(
                            height: 35,
                          ),
                          SizedBox(
                            height: 45,
                            width: double.infinity,
                            child: FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                color: Colors.blue,
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Menu()));
                                },
                                child: Text(
                                  "LOGIN",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                )),
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              Text("Belum Punya Akun?"),
                              Spacer(),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Register()));
                                },
                                child: Text(
                                  "Daftar Disini",
                                  style: TextStyle(color: Colors.red),
                                ),
                              )
                            ],
                          )
                        ])))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
