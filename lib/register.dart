import 'package:flutter/material.dart';

import 'package:smip_slawi_app/login.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            // resizeToAvoidBottomInset: false,
            body: Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.blue[400], Colors.white])),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30.0),
          child: Column(children: [
            SizedBox(
              width: double.infinity,
              child: Column(children: [
                SizedBox(
                  height: 80,
                ),
                Text(
                  "Silahkan registrasi",
                  style: TextStyle(fontSize: 35),
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "NAMA",
                    hintText: "NAMA ANDA",
                    suffixIcon: Icon(Icons.account_circle),
                  ),
                ),
                TextFormField(
                    decoration: InputDecoration(
                  labelText: "NO TELEPON",
                  hintText: "NO TELEPON ANDA",
                  suffixIcon: Icon(Icons.phone_android),
                )),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "EMAIL",
                    hintText: "EMAIL ANDA",
                    suffixIcon: Icon(Icons.email),
                  ),
                ),
                TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "PASSWORD",
                      hintText: "PASSWORD ANDA",
                      suffixIcon: Icon(Icons.lock),
                    )),
                TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "ULANGI PASSWORD",
                      hintText: "PASSWORD ANDA",
                      suffixIcon: Icon(Icons.lock),
                    )),
              ]),
            ),
            SizedBox(
              height: 35,
            ),
            SizedBox(
              height: 45,
              width: double.infinity,
              child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  color: Colors.blue,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login()));
                  },
                  child: Text(
                    "DAFTAR",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  )),
            ),
          ]),
        ),
      ),
    )));
  }
}
