import 'package:flutter/material.dart';
import 'package:smip_slawi_app/Edit.dart';

class Akun extends StatefulWidget {
  @override
  _AkunState createState() => _AkunState();
}

class _AkunState extends State<Akun> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.blue[400], Colors.white]),
        ),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(
                height: 70,
              ),
              Image.asset("assets/image/akun1.png", height: 100),
              SizedBox(height: 20),
              Container(
                height: 60,
                width: 300,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.blue),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Row(children: [
                    Text(
                      "Nama:",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                    Spacer(),
                    Text("Muhammad azhar syafiq",
                        style: TextStyle(fontSize: 15, color: Colors.white))
                  ]),
                ),
              ),
              SizedBox(height: 20),
              Container(
                height: 60,
                width: 300,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.blue),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Row(children: [
                    Text(
                      "Email:",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                    Spacer(),
                    Text("azharsyafiq77@gmail.com",
                        style: TextStyle(fontSize: 15, color: Colors.white))
                  ]),
                ),
              ),
              SizedBox(height: 20),
              Container(
                height: 60,
                width: 300,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.blue),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Row(children: [
                    Text(
                      "NO telepon",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                    Spacer(),
                    Text("085601388539",
                        style: TextStyle(fontSize: 15, color: Colors.white))
                  ]),
                ),
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: SizedBox(
                    height: 45,
                    width: 150,
                    child: FlatButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Edit()));
                        },
                        child: Text(
                          "Edit akun?",
                        ))),
              )
            ],
          ),
        ),
      ),
    );
  }
}
